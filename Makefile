.PHONY: run
run: build-go build-rust
	@python3 -m http.server

.PHONY: build-go
build-go:
	@cd go-highlight && \
	GOOS=js GOARCH=wasm go build -o ./hl.wasm 

.PHONY: build-rust
build-rust:
	@cd rust-highlight && \
	wasm-pack build --target web
