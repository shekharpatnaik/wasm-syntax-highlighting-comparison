# Comparison of highlighting via WASM vs using Highlight.js

In order to try to figure out whether we can replace our syntax highlighting library with a faster WASM version, I decided to benchmark different solutions out there.

Here are the results:

| Language     | Library Used                                   | Average Time |
| -------------| -----------------------------------------------|--------------|
| Baseline (js)| [Highlight.js](https://highlightjs.org/)       |  205ms       |
| Go           | [Chroma](https://github.com/alecthomas/chroma) |  4900ms      |
| Rust         | [Syntect](https://github.com/trishume/syntect) |  2800ms      |