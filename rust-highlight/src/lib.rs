use syntect::highlighting::ThemeSet;
use syntect::html::highlighted_html_for_string;
use syntect::parsing::SyntaxSet;
use wasm_bindgen::prelude::*;

#[wasm_bindgen]
pub fn highlight(data: &str) -> String {
    let ss = SyntaxSet::load_defaults_newlines();
    let ts = ThemeSet::load_defaults();

    let theme = &ts.themes["base16-ocean.dark"];
    let sr = ss.find_syntax_by_extension("js").unwrap();

    let html = highlighted_html_for_string(data, &ss, &sr, theme);
    return String::from(html);
}
