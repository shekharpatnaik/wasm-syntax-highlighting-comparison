//go:build js && wasm

package main

import (
	"bytes"
	"fmt"
	"syscall/js"

	"github.com/alecthomas/chroma/quick"
)

func main() {
	js.Global().Set("highlight", highlightWrapper())
	<-make(chan bool)
}

func highlight(input string) (string, error) {
	b := bytes.NewBufferString("")

	err := quick.Highlight(b, input, "javascript", "html", "monokai")
	if err != nil {
		return "", err
	}

	return b.String(), nil
}

func highlightWrapper() js.Func {
	return js.FuncOf(func(this js.Value, args []js.Value) any {
		if len(args) != 1 {
			return "Invalid no of arguments passed"
		}
		inputData := args[0].String()
		result, err := highlight(inputData)
		if err != nil {
			fmt.Printf("unable to convert to json %s\n", err)
			return err.Error()
		}
		return result
	})
}
